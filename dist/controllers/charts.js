"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoUtils_1 = require("../utils/mongoUtils");
exports.chartController = (req, res) => {
    mongoUtils_1.getDB().collection("drilldown_charts_data", (err, collection) => {
        if (err) {
            res.json(err);
        }
        else {
            collection.find().toArray((err2, results) => {
                if (err2) {
                    res.json(err2);
                }
                mongoUtils_1.getDB().collection("plotoptions_charts_data", (err3, collection2) => {
                    if (err3) {
                        res.json(err3);
                    }
                    else {
                        collection2.find().toArray((err4, results2) => {
                            if (err4) {
                                res.json(err4);
                            }
                            else {
                                res.json({
                                    "drillname": results,
                                    "plotoptions": results2
                                });
                            }
                        });
                    }
                });
            });
        }
    });
};
//# sourceMappingURL=charts.js.map