"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const mongodb_1 = __importDefault(require("mongodb"));
dotenv_1.default.config({
    path: ".env"
});
let _db;
exports.connectToMongoDB = () => new mongodb_1.default.MongoClient(process.env.MONGODB_URL)
    .connect()
    .then(conn => {
    console.log("> DB CONNECTED :");
    _db = conn.db(process.env.MONGODB_DB);
})
    .catch(err => {
    console.log("err :", err);
});
exports.getDB = () => _db;
//# sourceMappingURL=mongoUtils.js.map