# Seeding the mongodb collections

### There are as of now two collections present

- #### plotoptions_charts_data
- #### drilldown_charts_data


## plotoptions_charts_data
```bash
# Copy the below json object to your mongodb atlas or robo3t or any other GUI client of mongodb start with seed


/* 1 */
{
    "_id" : ObjectId("5cc1d73a3e6267d172aece92"),
    "name" : "Animals",
    "y" : 5,
    "drilldown" : "animals",
    "series_name" : "things"
}

/* 2 */
{
    "_id" : ObjectId("5cc1d76a3e6267d172aeceaa"),
    "name" : "Fruits",
    "y" : 2,
    "drilldown" : "fruits",
    "series_name" : "things"
}

/* 3 */
{
    "_id" : ObjectId("5cc1d7973e6267d172aecec4"),
    "name" : "Cars",
    "y" : 4,
    "drilldown" : "cars",
    "series_name" : "things"
}

/* 4 */
{
    "_id" : ObjectId("5cc1d7b43e6267d172aececd"),
    "name" : "Animals",
    "y" : 1,
    "drilldown" : "animals2",
    "series_name" : "things2"
}

/* 5 */
{
    "_id" : ObjectId("5cc1d7dd3e6267d172aecede"),
    "name" : "Fruits",
    "y" : 5,
    "drilldown" : "fruits2",
    "series_name" : "things2"
}

/* 6 */
{
    "_id" : ObjectId("5cc1d7fc3e6267d172aecef4"),
    "name" : "Cars",
    "y" : 2,
    "drilldown" : "cars2",
    "series_name" : "things2"
}

```


## drilldown_charts_data
```bash
# Copy the below json object to your mongodb atlas or robo3t or any other GUI client of mongodb start with seed


/* 1 */
{
    "_id" : ObjectId("5cc1d84c3e6267d172aecf12"),
    "id" : "animals",
    "name" : "Animals",
    "data" : [
        [
            "Cats",
            4
        ],
        [
            "Dogs",
            2
        ],
        [
            "Cows",
            1
        ],
        [
            "Sheep",
            2
        ],
        [
            "Pigs",
            1
        ]
    ]
}

/* 2 */
{
    "_id" : ObjectId("5cc1d8643e6267d172aecf26"),
    "id" : "fruits",
    "name" : "Fruits",
    "data" : [
        [
            "Apples",
            4
        ],
        [
            "Oranges",
            2
        ]
    ]
}

/* 3 */
{
    "_id" : ObjectId("5cc1d8713e6267d172aecf2e"),
    "id" : "cars",
    "name" : "Cars",
    "data" : [
        [
            "Toyota",
            4
        ],
        [
            "Opel",
            2
        ],
        [
            "Volkswagen",
            2
        ]
    ]
}

/* 4 */
{
    "_id" : ObjectId("5cc1d8803e6267d172aecf33"),
    "id" : "animals2",
    "name" : "Animals2",
    "data" : [
        [
            "Cats",
            3
        ],
        [
            "Dogs",
            5
        ],
        [
            "Cows",
            6
        ],
        [
            "Sheep",
            2
        ],
        [
            "Pigs",
            2
        ]
    ]
}

/* 5 */
{
    "_id" : ObjectId("5cc1d88d3e6267d172aecf3c"),
    "id" : "fruits2",
    "name" : "Fruits2",
    "data" : [
        [
            "Apples",
            1
        ],
        [
            "Oranges",
            5
        ]
    ]
}

/* 6 */
{
    "_id" : ObjectId("5cc1d89f3e6267d172aecf4d"),
    "id" : "cars2",
    "name" : "Cars2",
    "data" : [
        [
            "Toyota",
            2
        ],
        [
            "Opel",
            3
        ],
        [
            "Volkswagen",
            2
        ]
    ]
}

```




