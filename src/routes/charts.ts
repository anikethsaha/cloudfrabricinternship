import * as express from "express";
import { chartController } from "../controllers/charts";
const router = express.Router();

router.get("/data", chartController);

export default router;