/* tslint:disable */
import server from "../src/server";
import  chai from "chai";
import chaiHttp  from "chai-http";
import {connectToMongoDB} from '../src/utils/mongoUtils'


let should : Chai.Should = chai.should();
chai.use(chaiHttp);




describe('/GET /', () => {
    it('it should GET all the books', (done) => {
      chai.request(server)
          .get('/')
          .end((err, res) => {
                res.should.have.status(200);
            done();
          });
    });
});

describe("GET /charts/data", () => {
    it("Should return a object type", done => {
        chai.request(server)
        .get("/charts/data")
        .end((err,res) => {
            console.log('res.type', res.type)
            // res.should.have.status(200);
            res.body.should.be.a('object');
            done();
        });
    });
});



