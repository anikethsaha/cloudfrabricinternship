
import { getDB } from "../utils/mongoUtils";
import * as express from "express";

export const chartController  =
    (req: express.Request, res: express.Response): void => {
    getDB().collection("drilldown_charts_data", (err, collection) => {
        if (err) {
            res.json(err);
        }
        else {
            collection.find().toArray((err2, results) => {
                if (err2) {
                    res.json(err2);
                }
                getDB().collection("plotoptions_charts_data", (err3, collection2) => {
                    if (err3) {
                        res.json(err3);
                    } else {
                        collection2.find().toArray((err4, results2) => {
                            if (err4) {
                                res.json(err4);
                            } else {
                                res.json({
                                    "drillname" : results,
                                    "plotoptions" : results2
                                });
                            }
                        });
                    }
                });


            });
        }
    });
};