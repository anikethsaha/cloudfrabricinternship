"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const helmet_1 = __importDefault(require("helmet"));
const path_1 = __importDefault(require("path"));
const dotenv_1 = __importDefault(require("dotenv"));
const mongoUtils_1 = require("./utils/mongoUtils");
dotenv_1.default.config({ path: ".env" });
mongoUtils_1.connectToMongoDB();
const app = express_1.default();
app.use(helmet_1.default());
app.use(body_parser_1.default.urlencoded({ extended: true, limit: "50mb" }));
app.use(body_parser_1.default.json({ limit: "50mb" }));
app.set("port", (process.env.PORT || 8080));
app.set("trust proxy", true);
app.use(cors_1.default());
// V
// static files and views
app.use(express_1.default.static(path_1.default.join(__dirname, "../src/public")));
app.set("view engine", "ejs");
app.set("views", [
    path_1.default.join(__dirname, "../src/views"),
    path_1.default.join(__dirname, "../src/views/layouts/")
]);
app.engine("html", require("ejs").renderFile);
app.use(function (req, res, next) {
    next();
});
const charts_1 = __importDefault(require("./routes/charts"));
app.get("/", (req, res) => {
    res.render("index.ejs");
});
app.use("/charts", charts_1.default);
app.use((req, res, next) => res.status(404).send("404"));
app.listen(app.get("port"), () => {
    console.log("> Server is running on PORT ", app.get("port"));
});
exports.default = app;
//# sourceMappingURL=server.js.map